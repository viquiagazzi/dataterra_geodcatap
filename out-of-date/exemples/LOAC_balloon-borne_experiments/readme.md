# Notes for LOAC collection metadata file : datasets PTU and particles count. 

## Properties not presents on AERIS' LOAC balloon metadata document : https://services.aeris-data.fr/aeriscatalogueprod/metadata/194764ed-5d03-49f9-b4da-5ebe874982f6

+ For dcat:Distribution : dcat:accessURL, dct:description, dcat:downloadURL
+ For dcat:DataService : dcat:endpointDescription.

## Properties : content to improve. 

+ dcat:Dataset --> dct:description content is focused on the instrument but not in the data collection itself. Datasets very dependet on sensors !

## Vocabularies : content to improve. 

+ Concepts to LOAC (Light Optical Aerosol Counter) instrument is not present in Aeris sensors thesaurus. 
+ Add 'particles count' on the variables thesaurus Data Terra linked to the second dataset : Pcounts per particles size (concept not present in Voc Aeris). 

