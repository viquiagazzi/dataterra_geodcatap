+ mdb:MD_Metadata,  <!-- Namespaces -->

+ mdb:metadataIdentifier,  <!--  Champ automatique - Identifiant uuid de la fiche de MD -->
	+ mcc:code
	+ mcc:codeSpace

+ mdb:defaultLocale, <!--  Champ automatique - Langue des MD -->
	+ lan:LanguageCode (codeListValue="fre")
	+ lan:characterEncoding
	
+ mdb:parentMetadata, <!--  Champ DataTerra Dépôt rempli automatiquement : uuid de la fiche dépôt = fiche parente => Permet de relier les fiches de MD JDD à un dépôt  -->

+ mdb:metadataScope, <!--  Champ automatique   Permet d'identifier dans DataTerra les types de fiches de MD : Dépôt, JDD, Lien-Ressource  -->
	+ mdb:resourceScope (codeListValue="dataset")
	+ mdb:name
	
+ mdb:contact, <!--  Contact des MD - rôle custodian - rempli automatiquement => dataTerra  -->
	+ cit:role (codeListValue="custodian")
	+ cit:party, <!-- Organisation du contact role custodian  -->
	+ cit:address, <!--  Information d'adresses postale du contact rôle custodian  -->
	+ cit:electronicMailAddress, <!-- Adresse mail contact rôle custodian -->
	
+ mdb:contact, <!--  Information de contact des métadonnées rôle point de contact (récupéré automatiquement du contact de la ressource renseigné avec rôle poc)  -->
	+ cit:role (codeListValue="pointOfContact")
	+ cit:party, <!-- Organisation du contact role poc  -->
	+ cit:individual, <!--  Nom + prénom contact renseigné concatenation de deux champs  -->
	
+ mdb:dateInfo, <!--  Champ automatique Date de création de la fiche de MD -->
	+ cit:date
	+ cit:dateType (codeListValue="creation")
	
+ mdb:dateInfo, <!--  Champ automatique Date de révision = modification de la fiche de MD -->
	+ cit:date
	+ cit:dateType (codeListValue="revision")
	
+ mdb:metadataStandard, <!--  Champ automatique Standard de metadonnées  -->
	+ cit:title (ISO19115-3)
	
+ mdb:otherLocale, <!--  Champ automatique : langue de saisie des informations renseignées dans les MD  -->
	+ lan:languageCode (codeListValue="eng")
	+ lan:characterEncoding
	
+ mdb:metadataLinkage, <!--  Champ automatique - discussions en cours pour voir si on peut y mettre une URI pour la fiche de MD ou le service d'accès à la fiche de MD  -->
	+ cit:linkage (http://localhost:8080/geonetwork/srv/api/records/93b9fb67-644f-4628-8358-b598703b95b5)
	+ cit:function (codeListValue="completeMetadata")
	
+ mdb:referenceSystemInfo, <!--  Champ automatique : system de projection  -->
	+ mcc:code (http://www.opengis.net/def/crs/EPSG/0/4326)
	+ mrs:referenceSystemType (codeListValue="projected)
	
+ mdb:identificationInfo, <!--  Début d'information des ressources documentées sur la fiche de MD  -->
	+ mri:citation, <!-- Champ saisi : Titre du JDD - possibilite de saisie en FR et EN -->
		+ lan:textGroup, <!--  Information de langue de saisie du titre  -->
		+ cit:date, <!--  Champ saisi Date de création du JDD  -->
		+ cit:date, <!--  Champ automatique révision du JDD = mise à jour de la fiche de MD @ Christelle => A supprimer ??  -->
		+ cit:date, <!--  Champ automatique date de publication du JDD = quand la modération a validé la demande de publication  -->
		+ cit:identifier, <!--  Champ automatique : DOI attribué  -->
	+ mri:abstract, <!--  Champ saisi : Résmé du JDD  -->
		+ lan:textGroup, <!--  Information langue de saisie du résumé  -->
	+ mri:pointOfContact, <!--  Contact de la ressource rôle point de contact  -->
		+ cit:role (codeListValue="pointOfContact")
		+ cit:party, <!--  Champ saisi : organisation à laquelle appartient le contact rôle POC -->
		+ cit:address
		+ cit:electronicMailAddress
		+ cit:individual
	+ mri:pointOfContact, <!--  Contact de la ressource rôle auteur / chercheur (plusieurs) -->
		+ idem dernière balise
		+ cit:partyIdentifier, <!--  Champ saisi : identifiant de chercheur  -->
	+ mri:descriptiveKeywords, <!--  Mots clefs thématiques saisis  -->
		+ mri:keyword, <!--  URI du mot clef thématique saisi  + libelle langue fiche md selectionnee ici FR -->
		+ mri:type (codeListValue="thematic")
	+ mri:descriptiveKeywords, <!--  Mots clefs saisis  -->
		+ mri:keyword, <!--  URI du mot clef saisi  + libelle langue fiche md selectionnee ici FR -->
		+ mri:type (codeListValue="keyword")
	+ mri:descriptiveKeywords, <!--  Champs automatique : Type de fiche = données pour JDD /!\ Typologie n'est pas une entrée dans l'ISO cf. https://standards.iso.org/iso/19115/-3/mri/1.0/codelists.html /!\  Rappel du C4 de la norme : "Une extension est autorisée pour développer le nombre de valeurs dans une liste de codes. Cette partie de l’ISO 19115 utilise des listes de codes pour contrôler les vocabulaires. L’extension des listes de codes est déconseillée, même dans les profils. Losqu'elles doivent être étendues, il convient de minimiser le nombre d'entrées supplémentaires. De même, il convient que la liste de codes étendue soit publiée ou sinon mise à disposition. -->
		+ mri:keyword (<gco:CharacterString>donnée</gco:CharacterString>)
		+ mri:type (codeListValue="typologie")
	+ mri:resourceConstraints, <!--  Champ automatique : contraintes d'accès en fonction de présence d'embargo ou non = pas d'embargo => pas de contrainte d'accès -->
		+ mco:accessConstraints (codeListValue="unrestricted) 
		+ mco:otherConstraints, <!--  Champ saisi : contraintes d'utilisation par défaut CC BY pré-rempli  --> (gco:CharacterString>CC BY 4.0</gco:CharacterString)
	+ mri:defaultLocale, <!--  Champ automatique indiqué lors du choix de la langue de saisie de la fiche MD - Par défaut prend en compte la langue renseignée lors du dernier enregistrement. Si création fiche en FR puis modifiée ensuite en EN la langue renseignée sera EN -->
		+ lan:languageCode
		+ lan:characterEncoding
	+ mri:spatialRepresentationType, <!--  Champ saisi : type de représentation spatiale  -->
		+ mcc:MD_SpatialRepresentationTypeCode (codeListValue="vector")
	+ mri:topicCategory, <!--  Champ automatique rempli par défaut avec geoscientificInformation . A voir si on met ce champ pré-rempli lors de la saisi dans DataTerra (@christelle ?) et qu'on laisse la possibilité au déposant de choisir dans la liste présente dans la norme (cf. B.3.30 MD_TopicCategoryCode << Enumeration>> et https://standards.iso.org/iso/19115/-3/mri/1.0/codelists.html  -->
	+ mri:extent
		+ gex:temporalElement, <!--  Champ saisi  étendue temporelle des données  -->
			+ gml:TimePeriod (gml:beginPosition>2022-01-01</gml:beginPosition>	<gml:endPosition>2022-06-30</gml:endPosition>)
		+ gex:geographicElement, <!--  Champ saisi : decsription texte geographique des données  appel au service de geonames /!\ aucun lien avec la BBOX renseignée ensuite ou en parallèle -->
			+ gex:EX_GeographicBoundingBox
				+ Description
				+ gex:westBoundLongitude>
				+ gex:eastBoundLongitude>
				+ gex:southBoundLatitude>
				+ gex:northBoundLatitude>
				
+ mdb:distributionInfo
	+ mrd:transferOptions, <!--  Champs permettant de renseigner les liens et ressources associées au JDD  -->
		+ mrd:onLine
			+ cit:linkage, <!-- Champ saisi  : URL du lien/ressource  -->
			+ cit:protocol, <!--  Champ saisi "type de ressource" Issu d'une liste déroulante écrite dans le code JAVA. A discuter si besoin de pouvoir voir sur le front à quoi correspond chacun de ces termes  -->
			+ cit:name, <!--  Champ saisi : Nom du lien/ressource -->
			+ cit:description, <!--  Champ saisi : description du lien/ressource -->

+ mdb:resourceLineage, <!--  Champ saisi : lineage  -->
	+ mrl:statement
	+ mrl:scope, <!--  Précise que les informations s'appliquent à des JDD --> (codeListValue="dataset")
