# Notes on TCHR harvested example for Formater pole

##  Harvested metadata from SHOM

+ For dct:relation : Which is the real original metadata source ? From Formater catalog or shom catalog : metadata content may not be exactly the same. 

## Properties that can be improved 

+ dct:license can take the URI of the pdf license document of Etalab


## Properties not found

+ dcat:temporal : which temporal period for satellite derived products ? (automated + manual traitment).
	 Inputs images are detailed in descriptive documentation downloaded from URL in metadata.
	 
+ For dct:created, dct:modified. This property contains the date on which the Dataset (not metadata !) has been first created or lastly modified. 

+ For dcat:theme for variable HR Coastline : 'Limite haute du rivage'. 

+ Concerning properties that are related with derivated / calculated products : dcat:theme for sensors and platforms of inputs data/images ?
Description of the derived product must show the inputs, outputs, traitement chain, parameters, tools, versions ... -> sosa:Result, sosa:isResultOf (sosa:Observation), sosa:usedProcedure (sosa:Procedure) -> ssn:hasInput, ssn:hasOutput.  

+ For dcat:distribution : dct:description, dcat:accessURL not found. 

## Conformity 

+ Content in metadata related to : data services conformance and INSPIRE specs, how to model in geodcat-ap : 
	+ https://semiceu.github.io/GeoDCAT-AP/releases/2.0.0/#conformance-test-results
	+ https://semiceu.github.io/GeoDCAT-AP/releases/2.0.0/#conformity-and-data-quality---not-in-iso19115-core
    
