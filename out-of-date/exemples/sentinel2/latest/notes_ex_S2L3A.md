# elements reviewed 22/04/05

+ Le dct:temporal à retenir pour la collection  est celui associé à la date d'acquisition dans le XML("<ACQUISITION_DATE>2018-09-15T00:00:00.000Z</ACQUISITION_DATE>"), qui est également celui présent dans le nom du fichier de la distribution disponible sous l'API theia.cnes : 2018-09-15T00:00:00.000Z 
	
+ Propriété dcat:spatialResolutionInMeters est défini au niveau des datasets bandes / masques de manière unique, et avec les 2 valeurs au niveau du dataset collection. 

+ Intégration de la taille des images en ncols, nrows (pixels). Piste : dqv:hasQualityMeasurement. 

+ Intégration des metadata concernant : chaîne traitement et software utilisés. Piste : sosa:usedProcedure a sosa:Procedure. 

+ Intégration des index de qualité par rapport à la couverture nuageuse et neigeuse. Piste : dqv:hasQualityMeasurement pour le Dataset collection en récupérant le type de couverture et sa valeur (poucentage : decimal) dans un concept de  type dqv:Metric.
 
+ L'image Quicklook livré n'est pas intégrée, pas besoin pour la découverte des jeux de données. 


# métadonnées S2L3 présents dans fichier SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_MTD_ALL.xml Modèle MUSCATE.

## Dataset
+ <IDENTIFIER>SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C</IDENTIFIER>
+ <AUTHORITY>THEIA</AUTHORITY>
+ <PRODUCER>MUSCATE</PRODUCER>
+ <PROJECT>SENTINEL2</PROJECT>
+ <GEOGRAPHICAL_ZONE type="Tile">T31TEJ</GEOGRAPHICAL_ZONE>

## Product

+ <PRODUCT_ID>SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2</PRODUCT_ID>
+ <ACQUISITION_DATE>2018-09-15T00:00:00.000Z</ACQUISITION_DATE>
+ <PRODUCTION_DATE>2021-06-29T16:42:12.120Z</PRODUCTION_DATE>
+ <PRODUCT_VERSION>2.2</PRODUCT_VERSION>
+ <PRODUCT_LEVEL>L3A</PRODUCT_LEVEL>
+ <PLATFORM>SENTINEL2X</PLATFORM>
+ <ORBIT_NUMBER type="Orbit">-1</ORBIT_NUMBER>

+ <Band_Global_List count="10">
	<BAND_ID>B2</BAND_ID>
	<BAND_ID>B3</BAND_ID>
	<BAND_ID>B4</BAND_ID>
	<BAND_ID>B5</BAND_ID>
	<BAND_ID>B6</BAND_ID>
	<BAND_ID>B7</BAND_ID>
	<BAND_ID>B8</BAND_ID>
	<BAND_ID>B8A</BAND_ID>
	<BAND_ID>B11</BAND_ID>
	<BAND_ID>B12</BAND_ID>
	
+ <Band_Group_List> Resolution 10m
	<Group group_id="R1">
	<Band_List count="4">
	<BAND_ID>B2</BAND_ID>
	<BAND_ID>B3</BAND_ID>
	<BAND_ID>B4</BAND_ID>
	<BAND_ID>B8</BAND_ID>
	
+ <Band_Group_List> Resolution 20m
	<Group group_id="R2">
	<Band_List count="6">
	<BAND_ID>B5</BAND_ID>
	<BAND_ID>B6</BAND_ID>
	<BAND_ID>B7</BAND_ID>
	<BAND_ID>B8A</BAND_ID>
	<BAND_ID>B11</BAND_ID>
	<BAND_ID>B12</BAND_ID>
	
+ <QUICKLOOK bands_id="B4,B3,B2,">./SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_QKL_ALL.jpg</QUICKLOOK>

## Images, FRC : Flat Relfectance Composite

+ <Image_Properties>
	<NATURE>Flat_Reflectance_Composite</NATURE>
	<FORMAT>image/tiff</FORMAT>
	<ENCODING>int16</ENCODING>
	<ENDIANNESS>LittleEndian</ENDIANNESS>
	<COMPRESSION>None</COMPRESSION>

+ <Image_File_List>
	<IMAGE_FILE band_id="B11">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B11.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B12">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B12.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B2">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B2.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B3">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B3.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B4">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B4.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B5">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B5.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B6">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B6.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B7">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B7.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B8">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B8.tif</IMAGE_FILE>
	<IMAGE_FILE band_id="B8A">SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FRC_B8A.tif</IMAGE_FILE>

## Masks. FLG : Pixel Status Flag, WGT : Pixel Weight, DTS : Weighted Average Dates

+ <Mask_Properties>
	<NATURE>Pixel_Status_Flag</NATURE>
	<FORMAT>image/tiff</FORMAT>
	<ENCODING>byte</ENCODING>
	<ENDIANNESS>LittleEndian</ENDIANNESS>
	<COMPRESSION>DEFLATE</COMPRESSION>
	</Mask_Properties>
	<Mask_File_List>
	<MASK_FILE group_id="R1">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FLG_R1.tif</MASK_FILE>
	<MASK_FILE group_id="R2">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_FLG_R2.tif</MASK_FILE>
	
+ <Mask_Properties>
	<NATURE>Pixel_Weight</NATURE>
	<FORMAT>image/tiff</FORMAT>
	<ENCODING>int16</ENCODING>
	<ENDIANNESS>LittleEndian</ENDIANNESS>
	<COMPRESSION>DEFLATE</COMPRESSION>
	</Mask_Properties>
	<Mask_File_List>
	<MASK_FILE group_id="R1">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_WGT_R1.tif</MASK_FILE>
	<MASK_FILE group_id="R2">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_WGT_R2.tif</MASK_FILE>
	
+ <Mask_Properties>
	<NATURE>Weighted_Average_Dates</NATURE>
	<FORMAT>image/tiff</FORMAT>
	<ENCODING>int16</ENCODING>
	<ENDIANNESS>LittleEndian</ENDIANNESS>
	<COMPRESSION>DEFLATE</COMPRESSION>
	</Mask_Properties>
	<Mask_File_List>
	<MASK_FILE group_id="R1">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_DTS_R1.tif</MASK_FILE>
	<MASK_FILE group_id="R2">MASKS/SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2_DTS_R2.tif</MASK_FILE>
	
## Coordinate Reference System : WGS 84 / UTM zone 31N

+ <Global_Geopositioning>
	<Point name="upperLeft">
		<LAT>44.2534175806</LAT>
		<LON>2.99974947412</LON>
		<X>499980.0</X>
		<Y>4900020.0</Y>
	</Point>
	<Point name="upperRight">
		<LAT>44.2451424114</LAT>
		<LON>4.37494005874</LON>
		<X>609780.0</X>
		<Y>4900020.0</Y>
	</Point>
	<Point name="lowerRight">
		<LAT>43.2567942937</LAT>
		<LON>4.35249403248</LON>
		<X>609780.0</X>
		<Y>4790220.0</Y>
	</Point>
	<Point name="lowerLeft">
		<LAT>43.2647903769</LAT>
		<LON>2.99975356551</LON>
		<X>499980.0</X>
		<Y>4790220.0</Y>
		</Point>
	<Point name="center">
		<LAT>43.7570922676</LAT>
		<LON>3.68173431429</LON>
		<X>554880.0</X>
		<Y>4845120.0</Y>
	</Point>

## Quality products ; indexes Snow / Clouds.

+ 	<Product_Quality_List level="N3">
	<Product_Quality>
	<Source_Product>
	<PRODUCT_ID>SENTINEL2X_20180915-000000-000_L3A_T31TEJ_C_V2-2</PRODUCT_ID>
	<ACQUISITION_DATE>2018-09-15T00:00:00.000Z</ACQUISITION_DATE>
	<PRODUCTION_DATE>2021-06-29T16:42:12.120Z</PRODUCTION_DATE>
	</Source_Product>
	<Global_Index_List>
	<QUALITY_INDEX name="CloudPercent">0</QUALITY_INDEX>
	<QUALITY_INDEX name="SnowPercent">0</QUALITY_INDEX>
	</Global_Index_List>
	</Product_Quality>


## Production information : jobs, software, inputs S2L2A, parameters (calibration, TRT), 

+ <Job>
	<WORKPLAN_ID>synthese_temporelle_20210629_9</WORKPLAN_ID>
	<STEP_ID>synthese_temp</STEP_ID>
	<JOB_ID>1</JOB_ID>
	<PRODUCER_NAME>MUSCATE</PRODUCER_NAME>
	<START_PRODUCTION_DATE>2021-06-29T14:24:04.288Z</START_PRODUCTION_DATE>
	<END_PRODUCTION_DATE>2021-06-29T16:43:21.945Z</END_PRODUCTION_DATE>
	
+ <Production_Facility>
	<SOFTWARE_NAME>WASP</SOFTWARE_NAME>
	<SOFTWARE_VERSION>1.2.2</SOFTWARE_VERSION>
	
+ <Inputs_List>
	<PRODUCT>
	<PRODUCT_ID>SENTINEL2B_20180831-104845-762_L2A_T31TEJ_C_V2-2</PRODUCT_ID>
	<ACQUISITION_DATE>2018-08-31T10:48:45.762Z</ACQUISITION_DATE>
	<PRODUCTION_DATE>2021-05-05T22:54:58.430Z</PRODUCTION_DATE>
	</PRODUCT>
...
	<PRODUCT>
	<PRODUCT_ID>SENTINEL2B_20180930-104411-965_L2A_T31TEJ_C_V2-2</PRODUCT_ID>
	<ACQUISITION_DATE>2018-09-30T10:44:11.965Z</ACQUISITION_DATE>
	<PRODUCTION_DATE>2021-05-11T05:46:00.132Z</PRODUCTION_DATE>
	
+ <Parameter>
	<PARAMETER_FILE>param_trt_l3A_S2_J15_5ATT_23DF_1PM_1.3/param_trt_l3A_S2_J15_5ATT_23DF_1PM.xml</PARAMETER_FILE>
	<NATURE>PARAM_SYNTHESE_TRT</NATURE>
  <Parameter>
	<PARAMETER_FILE>PARAM_CALIBRATION_L3A_S2_1.0/param_calibration_l3A.xml</PARAMETER_FILE>
	<NATURE>PARAM_SYNTHESE_CALIBRATION</NATURE>
	
## License : Etalab 2.0

https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf
