# Notes on FLATSIM products (interferometry) example for Formater pole

## Data Services (download, WMS) not detailed on metadata.

+ Data services description : https://www.mdpi.com/remotesensing/remotesensing-13-03734/article_deploy/html/images/remotesensing-13-03734-g010.png
 paper : https://www.mdpi.com/2072-4292/13/18/3734/htm
 
 + For dcat:Distribution, the dcat:downloadURL not present in metadata file due to an access restriction. 
 
## Vocabularies URIs

+ For variables related to Datasets : 3 packages on distributions
		+ Interferograms : APS (atmospheric model), differential interferograms (filtered), spatial coherence.
		+ Timeseries : phase delay at timestep, mean LOS velocity, time series quality products
		+ Auxiliady data : processing parameters. 


