# Classes on Sextant (Odatis) API OGC Records implementation*

| Class Name          | Present for Data Terra |  Notes |
|---------------------|------------------------|--------|
| dcat:CatalogRecord | Yes ||
| dcat:Dataset | Yes |  |
| dcat:Distribution | Yes ||
| vcard:Kind | Yes | for dcat:contactPoint |
| skos:Concept | Yes | for dcat:theme  (Dataset) and adms:representationTechnique (Distribution) |
| prov:Activity | No | for prov:wasUsedBy (Dataset) |
| foaf:Document | Yes | Instead dcat:landingPage (Dataset) |
| dct:ProvenanceStatement | No | for dct:provenance (Dataset) |

* [OGC API Core Queryables record properties](http://docs.ogc.org/DRAFTS/20-004.html#core-queryables)

# CatalogRecord properties implemented

| Property      | Present for Data Terra | Notes |
|---------------|------------------------|-------|
| foaf:primaryTopic | Yes | LInks to the dcat:Dataset resource |
| dct:modified | Yes | |
| dct:identifier | Yes ||
| dct:created | Yes ||
| dct:language | Yes | vocabulary : <http://publications.europa.eu/resource/authority/language/> |

# Dataset properties implemented

| Property      | Present for Data Terra | Notes |
|---------------|------------------------|-------|
| dct:title | Yes | |
| dct:description | Yes | |
| dct:identifier | Yes | URIs' main identifier in the context of the catalog |
| dct:language | Yes ||
| dct:created | Yes ||
| dct:modified | Yes ||
| dcat:contactPoint | Yes ||
| dct:type | Yes ||
| prov:wasUsedBy | No | MAY be used to specify a testing Activity over a Dataset, against a given Standard, producing as output a conformance degree. |
| dcat:distribution | Yes ||
| dct:accrualPeriodicity | No | This property refers to the frequency at which the Dataset is updated. |
| dct:provenance | Yes ||

# Properties GeoDCAT not implemented

| Property      | Present for Data Terra | Notes |
|---------------|------------------------|-------|
| dct:accessRights (Dataset, Distribution, DataService) | Yes (R) | Not present at any level |
| dct:license (Distribution) | Yes (M) | |
| dct:rights (Distribution) | Yes (O) | |
