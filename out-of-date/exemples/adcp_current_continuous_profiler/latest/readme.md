# Properties that are not presents in the Odatis ADCP Coriolis metadata document :

+ dct:license <License_URI> ; # This property refers to the licence under which the Distribution is made available.
+ dcat:downloadURL <https://...> ; # This property contains a URL that is a direct link to a downloadable file in a given format.
+ FTP dcat:accessService --> dcat:endpointDescription <endpointDescription_URI> ] ] ; # This property contains a description of the Data Services available via the endpoints, including their operations, parameters etc.
+ dct:temporal --> #dcat:endDate "YYYY-MM-DDThh:mm:ss.s"^^xsd:dateTime ] ; FTP date : 2021, mais en fiches de MTD 2019. 
+ dcat:theme <sensor_URI> ; #Sensor thesaurus
+ dcat:theme <platform_URI> ; #Platform thesaurus
+ dcat:qualifiedRelation --> items des données par années ([2001](https://sextant.ifremer.fr/Donnees/Catalogue#/metadata/c0278149-6d1b-4198-9b6a-a8b617fb7ae6) -> [2019](https://sextant.ifremer.fr/Donnees/Catalogue#/metadata/59403137-7fc9-45b7-9b1b-e63883f3c4b8)). Inutile de fournir ce niveau de granularité dans le catalogue Data Terra. Ceci sera accessible à partir de la fiche au niveau du pôle Odatis. 
