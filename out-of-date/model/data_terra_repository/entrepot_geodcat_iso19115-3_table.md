# Mapping table Data Terra geoDCAT implementation for repository dataset exemple in ISO 19115:2014.

| Repo ex. ISO | geoDCAT props (domain)  | Discovery ISO (Annex F) | Discovery Data Terra | Notes |
|:-----------|:---------------------|---------------|----------------------|-------|
| **metadataIdentifier** | identifier (CatalogRecord) | Y | Y (M) | Metadata record identifier |
| **defaultLocale** | language (CatalogRecord) | N | Y (R) | Metadata languages : default and others |
| **parentMetadata** | qualifiedRelation (Dataset) | N | Y | Not spec. available for CatalogRecord class | 
| **metadataScope** | primaryTopic (CatalogRecord) | Y | Y (M) | links the Catalogue Record to the Dataset, Data service or Catalogue described in the record | 
| **contact** (custodian) | custodian (CatalogRecord) | N | Y (O) ||
| **contact** (poc)| contactPoint (CatalogRecord) | Y | Y (O) ||
| **dateInfo** (creation)| created (CatalogRecord) | Y | Y (R) ||
| **dateInfo** (update)| modified (CatalogRecord) | Y | Y (M) ||
| **metadataStandard** | conformsTo (CatalogRecord) | N | Y (R) ||
| **otherLocale** | | N || See *defaultLocale* |
| **metadataLinkage** | source (CatalogRecord) | N | Y (R) | This property refers to the original metadata record that was used in creating the Catalogue Record |
| **referenceSystemInfo** | spatial (Dataset) | N | Y (M) | Reference system for the Dataset |
| **identificationInfo** | (Dataset) | Y | Y (M) | |
| *_citation* | title (Dataset) | Y | Y (M) ||
| *_abstract* | description (Dataset) | Y | Y (M) ||
| *_pointOfContact* | contactPoint (Dataset) | Y | Y (R) ||
| *_descriptiveKeywords* | theme (Dataset) | Y | Y (R) | all sort of controlled vocabularies |
| *_descriptiveKeywords* | keywords (Dataset) | Y | Y (R) | Keyword's entries |
| *_resourceConstraints* | accessRights (Dataset) | Y | Y (O) ||
| *_defaultLocale* | language (Dataset) | N | Y (O) | Dataset languages : default and others |
| *_spatialRepresentationType* | representationTechnique (Distribution) | N | Y (O) ||
| *_topicCategory* | dct:subject (Dataset)| Y | Y (R) | *ONLY* : [URIs minted for the ISO code list in the INSPIRE Registry](http://inspire.ec.europa.eu/metadata-codelist/TopicCategory) |
| *_extent* | saptial / temporal (Dataset) | Y | Y (R) | |
| **distributionInfo** | (Distribution) | N | Y (R) ||
| *_onLine* | accessURL / description / title / conformsTo (Distribution) | N | Y | Property depends on the type of online resource |
| **resourceLineage** | provenance (Dataset) | Y | Y (O) | |

This mapping table takes into account the [Comparison between INSPIRE and ISO 19115-1:2014](https://semiceu.github.io/GeoDCAT-AP/drafts/latest/#comparison-between-inspire-and-iso19115-12014) and [ISO 19115 Mappings](https://semiceu.github.io/GeoDCAT-AP/releases/2.0.0/#inspire-and-iso-19115-mappings) from GeoDCAT-AP V2 recommendation. 

# Data Terra geoDCAT --> for repository exemple. 

+ For **CatalogRecord** class, any recommendations yet ! 
+ **Dataset** 		--> all identified GeoDCAT mandatory properties are presents
+ **Distribution** 	
	+ license (M) : If different from dataset's acces rights.
+ **DataService** --> Not needed in repository's context. 

# ISO exemple content not found in GeoDCAT classes

+ **parentMetadata** for CatalogRecord
+ Not **referenceSystemInfo** for CatalogRecord, but for Dataset : dct:spatial --> Problem with fixed CRS at CatalogRecord level ??
+ *_topicCategory* for Dataset could be viewed as a dcat:theme property ?



