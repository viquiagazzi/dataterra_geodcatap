# [Resource](https://docs.google.com/document/d/1aN3lxIirmM8b78moxDRVvLMvui2VGOQceb_ML_TMe_Q) from GT catalog Data Terra. 

# Granularity datasets from Data Terra centers' exemples :

## Aeris

Metadata records : 2 in situ datasets (balloon and aircraft)

+ LOAC balloon campaign (in situ). 
	+ Spatio-temporal series.
	+ Resolution : ?
	+ Granularity : collection made by 2 base datasets, PTU and particle's count.
	+ Variables : mesurements for PTU variables and particles count aggregation per altitude and particle's size.
	
+ CALIOP aircraft datasets (in situ). Calipso products. 
	+ Spatio-temporal series. Product level 2 : https://www.icare.univ-lille.fr/calipso/products/
	+ Resolution 1km for clouds layer, 10 layers gridded. 
	+ Granularity : 1 collection dataset with 10 layers from 2006.
	+ Variables : for eache layer : "Résultat de la classification (cf. DP 2.1D) Altitudes haute et basse (km) Signal lidar rétrodiffusé intégré sur la couche (km-1.sr-1) à 532 et 1064nm Taux de dépolarisation intégré sur la couche(1) Epaisseur optique à 532nm(1) (tau=10-3 à 5)".  


## Odatis

Metadata records : 2, in situ from ship and satellite altimetry derived product. 

+ Mesoscale eddy trajectory atlas. Derived from satellite altimetry data.
	+ Granularity of 3 datasets : 1 collection + 2 spatio-temporal gridded datasets. 
	+ 2 distinct products : NRT and DT.
	+ Variables : "Les variables fournies incluent la localisation du centre du tourbillon, jour par jour, le type (cyclonique/anticyclonique), la vitesse sur le contour et le rayon."

+ current profilers measured continuously by hull-mounted ADCPs (Acoustic Doppler Current Profiler). Coriolis product.
	+ Granularity of unique dataset level : parent record linked to child records datasets by year (every platform aggregated ). Access to traitements' reports cathegorised by platform ship.  
	+ Variables : "3 composantes spatiales de la vitesse du courant sur toute la colonne d’eau jusqu’à des profondeurs pouvant atteindre 1500m, dans des cellules définies temporellement."

## Theia satellite

Metadata records : 3 (S1 altymetry, S2 multispectral imagery, and S2 synthese product)

+ SENTINEL-1 IW_GRDH Level-1C User Product. Dual polarization : VV and VH. 
	+ Granularity of 3 datasets : 1 collection and 2 polarized bands.
	+ Variables : surface reflectance from C-band.
	
+ Sentinel 2 MSI image L1C level product
	+ Granularity of 14 datasets : 1 collection for entire image and 1 dataset per band.
	+ Variables : TOA reflectance, https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-2/data-products
l
+ Sentinel 2 MSI image L3A level product. Continious mosaic.
	+ Granularity of 17 datasets : collection level plus 10 bands and 6 masks. 
	+ Product : Level 3A datasets for Sentinel-2 are monthly, cloudless, surface reflectance syntheses.

## Formater TODO

Metadata records : 2 satellite derived products (one semi-automatic).

+ TCHR from Shom : 
	+ Granilarity : collection level from TAFF (not only for french_subantarctic_islands )
	+ Variables : coastline, altitude and planimetric position. 

+ FLATSIM interferometry (satellite S1 derived product) :
	+ Granularity : gridded collection time series for specific location (Tarim).
	+ Variables : Interferometry, timeseries, auxiliary data. (details : https://formater.pages.in2p3.fr/flatsim/#flatsim-product-manual). 



# Elements for consistency in datasets granularity

## Same product in temporal dataseries collection 

+ Unique dataset for collection level, in order to avoid values' dupplication. 
+ How to manage differents platforms/sensors with their associated resources when parent dataset is an aggregated dataset ?

## Same product with spatial distribution collection : spatial variable

+ unique dataset with bbox polygone in dct:spatial
+ records by station / 	buoy (sensor)

## Collection of different products

+ A record for parent (satellite image / Eddies atlas ), other for child (image band, mask / NRT or DT product ) .


