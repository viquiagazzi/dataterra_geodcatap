# Core classes for Data Terra descovery ontology : elementary classes and properties for descovery use case.

Source : [GeoDCAT-AP ontology V2](https://semiceu.github.io/GeoDCAT-AP/releases/2.0.0/). 

General constraints --> M : Mandatory, R : Recommended, O : Optional

| Class Name          | Constraint GeoDCAT-AP V2 | Constraint Data Terra | URI | Notes |
|---------------------|--------------------------|-----------------------|-----|-------|
| CatalogRecord       |  O     | O				 | [dcat:CatalogRecord](http://www.w3.org/ns/dcat#CatalogRecord) |  A description of a Catalogue's, Data Service's, or Dataset's entry in the Catalogue. |
| Dataset             |  M     | M               | [dcat:Dataset](http://www.w3.org/ns/dcat#Dataset) | A conceptual entity that represents the information published. Mainly collection levels, but with differents granularities.  |
| Distribution        |  R     | M               | [dcat:Distribution](http://www.w3.org/ns/dcat#Distribution) |  |
| Location            |  O     | M               | [dct:Location](http://purl.org/dc/terms/Location) | May not exists for Atmospheric simulation chambers |
| PeriodOfTime        |  O     | M               | [dct:PeriodOfTime](http://purl.org/dc/terms/PeriodOfTime) |  |
| Kind (Organization) |  O     | M               | [vcard:Organization](http://www.w3.org/2006/vcard/ns#Organization) |  |
| Data Service        |  O     | R               | [dcat:DataService](http://www.w3.org/ns/dcat#DataService) |  |
| Relationship        |  O     | R               | [dcat:Relationship](http://www.w3.org/ns/dcat#Relationship) | Multiple roles for the relation property (dcat:hadRole)  |
| Agent               |  R     | R               | [foaf:Agent](http://xmlns.com/foaf/spec/#term_Agent) |  |
| RightsStatement     |  O     | R               | [dct:RightsStatement](http://purl.org/dc/terms/RightsStatement) |  |

## CatalogRecord properties

| Property                       | Constraint GeoDCAT-AP V2 | Constraint Data Terra | Range                       | Vocabulary (GeoDACT-AP, Data Terra)                                           |
|--------------------------------|--------------------|-----------------|-----------------------------------------|-------------------------------------------------------------------------------|
| dct:modified                   | M                  | M               | xsd:date or xsd:dateTime                | --                                                                            |
| dct:primaryTopic               | M                  | M               | dcat:Dataset or dcat:Dataservice or dcat:Catalog | --                                                                   |
| dcat:identifier				 | R 				  | R 				| rdfs:Literal |  |
| dcat:created 					 | O 				  | O 				| xsd:date or xsd:dateTime | |
| dcat:language 				 | O 				  | O 				| dct:LinguisticSystem ||
| dct:conformsTo 				 | R 				  | R				| dct:Standard | For GeoDCAT-AP v2.0 --> <http://data.europa.eu/930/> |

## Dataset properties
 
| Property                       | Constraint GeoDCAT-AP V2 | Constraint Data Terra | Range                       | Vocabulary (GeoDACT-AP, Data Terra)                                           |
|--------------------------------|--------------------|-----------------|-----------------------------------------|-------------------------------------------------------------------------------|
| dct:description                | M                  | M               | rdfs:Literal                            | --                                                                            |
| dct:title                      | M                  | M               | rdfs:Literal                            | --                                                                            |
| dcat:contactPoint              | R                  | M               | vcard:Kind  (Organisation, Individual)  | -- |
| dcat:distribution              | R                  | M               | dcat:Distribution                       | --                                                                            |
| dcat:keyword                   | R                  | M               | rdfs:Literal                            | --                                                                            |
| dct:spatial                    | R                  | M               | dct:Location                            | [Geonames](http://sws.geonames.org/), [EUV-CONT](http://publications.europa.eu/resource/authority/continent), [EUV-COUNTRIES](http://publications.europa.eu/resource/authority/country), [EUV-PLACES](http://publications.europa.eu/resource/authority/place)                    |
| dct:temporal                   | R                  | M               | dct:PeriodOfTime                        | --                                                                            |
| dcat:theme                     | R                  | M               | skos:Concept                            | [TVI](https://terra-vocabulary.org/ncl/FAIR-Incubator), [EUV-THEMES](http://publications.europa.eu/resource/authority/data-theme), [INSPIRE-THEMES](http://inspire.ec.europa.eu/theme)  | 
| dct:accessRights               | O                  | R               | dct:RightsStatement                     | [EUV-AR](http://publications.europa.eu/resource/authority/access-right), [INSPIRE-LPA](http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess) |
| dct:publisher                  | R                  | R               | foaf:Agent                              | [EUV-CB](http://publications.europa.eu/resource/authority/corporate-body)     |
| dcat:spatialResolutionInMeters | O                  | R               | xsd:decimal                             | -- |
| dcat:qualifiedRelation         | O                  | R               | dcat:Relationship                       | -- |
| dct:identifier                 | O                  | R               | rdfs:Literal                            | --                                                                            |
| dct:type                       | O                  | O               | skos:Concept                            | [INSPIRE-RT](https://inspire.ec.europa.eu/metadata-codelist/ResourceType) |
| dct:conformsTo (Ref. Sys.)     | O                  | O               | dct:Standard                            | [OPENGIS](http://www.opengis.net/def/crs/EPSG/0/)                             |
| dcat:landingPage               | O                  | O               | foaf:Document                           | --                                                                            |
| foaf:page                      | O                  | O               | foaf:Document                           | --                                                                            |
| dct:creator                    | O                  | O               | foaf:Agent                              | --                                                                            |
| dct:created                    | O                  | O               | rdfs:Literal (xsd:date or xsd:dateTime) | -- |
| dct:modified                   | O                  | O               | rdfs:Literal (xsd:date or xsd:dateTime) | -- |
| dct:issued                     | O                  | O               | rdfs:Literal (xsd:date or xsd:dateTime) | -- |
| owl:versionInfo                | O                  | O               | rdfs:Literal                            | -- |
| geodcat:principalInvestigator  | O                  | O               | foaf:Agent                              | -- |
| dct:language                   | O                  | O               | dct:LinguisticSystem | [EUV-LANG](http://publications.europa.eu/resource/authority/language/) |
| dct:provenance                 | O                  | O               | dct:ProvenanceStatement                 | -- |


## Distribution properties

| Property               | Type GeoDCAT-AP V2 | Type Data Terra | Range                 | Vocabulary (GeoDACT-AP, Data Terra)                                  |
|------------------------|--------------------|-----------------|-----------------------|----------------------------------------------------------------------|
| dcat:accessURL         | M                  | M               | rdfs:Resource         | --                                                                   | 
| dct:description        | R                  | M               | rdfs:Literal          | --                                                                   |
| dct:license            | R                  | M               | dct:LicenseDocument   | --                                                                   |
| dct:format             | R                  | R               | dct:MediaTypeOrExtent | [EUV-FT](http://publications.europa.eu/resource/authority/file-type) |
| dcat:byteSize          | O                  | O               | xsd:decimal           | --                                                                   |
| dcat:compressFormat    | O                  | O               | dct:MediaType         | [IANA Media Types](https://www.iana.org/assignments/media-types)     |
| dcat:downloadURL       | O                  | O               | rdfs:Resource         | --                                                                   |
| dcat:mediaType         | O                  | O               | dct:MediaType         | [IANA Media Types](http://www.iana.org/assignments/media-types)      |
| dct:rights             | O                  | O               | dct:RightsStatement   | --                                                                   |
| dcat:accessService     | O                  | O               | dcat:DataService      | --                                                                   |
| cnt:characterEncoding  | O                  | O               | xsd:string            | --                                                                   |
| adms:representationTechnique | O            | O               | skos:Concept          | [INSPIRE-SRT](https://inspire.ec.europa.eu/metadata-codelist/SpatialRepresentationType) |

## DataService properties (*)

| Property                 | Type GeoDCAT-AP V2 | Type Data Terra | Range         | Vocabulary (GeoDACT-AP, Data Terra)                                        |
|--------------------------|--------------------|-----------------|---------------|----------------------------------------------------------------------------|
| dcat:endpointURL         | M                  | M               | rdfs:Resource |--                                                                          |
| dct:title                | M                  | M               | rdfs:Literal  |--                                                                          |
| dct:description          | O                  | R               | rdfs:Literal  |--                                                                          |
| dcat:endpointDescription | R                  | R               | rdfs:Resource |--                                                                          |
| dct:conformsTo (protocol)          | O        | O     | dct:Standard  | [INSPIRE-PV](https://inspire.ec.europa.eu/metadata-codelist/ProtocolValue) |

(*) Properties can be extended with [Hydra](http://www.w3.org/ns/hydra/core#) ontology for describing data service's operations : for supported operation description (hydra:supportedOperation) or the output format of the operation (hydra:returns). 

## Location properties

| Property                 | Type GeoDCAT-AP V2 | Type Data Terra | Range         | Vocabulary (GeoDACT-AP, Data Terra)                                        |
|--------------------------|--------------------|-----------------|---------------|----------------------------------------------------------------------------|
| dcat:bbox | R | M | rdfs:Literal | -- |
| locn:geometry (geometries) | O | O | rdfs:Literal typed as gsp:wktLiteral or gsp:gmlLiteral | -- |

## PeriodOfTime properties

| Property                 | Type GeoDCAT-AP V2 | Type Data Terra | Range         | Vocabulary (GeoDACT-AP, Data Terra)                                        |
|--------------------------|--------------------|-----------------|---------------|----------------------------------------------------------------------------|
| dcat:startDate         | R*                  | M               | rdfs:Literal typed as xsd:date or xsd:dateTime |--                                           |
| dcat:endDate         | R*                  | R               | rdfs:Literal typed as xsd:date or xsd:dateTime |--                                           |

* Please note that while both properties are recommended, one of the two must be present for each instance of the class dct:PeriodOfTime, if such an instance is present.

## Relationship properties

| Property     | Type GeoDCAT-AP V2 | Type Data Terra | Range         | Vocabulary (GeoDACT-AP, Data Terra)                        |
|--------------|--------------------|-----------------|---------------|------------------------------------------------------------|
| dcat:hadRole | M                  | M               | dcat:Role     | [IANA-RELATION](https://www.iana.org/assignments/relation) |
| dct:relation | M                  | M               | rdfs:Resource | --                                                         |

# Domain-specific complementary ontologies.

| Ontology name | Domain | URL spec. | namespace : URI | Data Nature | Schema |
|---------------|--------|-----------|-----------------|-------------|--------|
| Earth Observation Ontology | Dedicated to represents metadata of Sentinel images. It mainly describes the product (an image) as a result of an observation. | http://melodi.irit.fr/ontologies/eom/ | eom : http://melodi.irit.fr/ontologies/eom# | Satellite (Sentinel) outputs. | Disponible dans [ce livrable](http://www.candela-h2020.eu/sites/default/files/candela/public/content-files/deliverables/D2.6%20Semantic%20Search%20v2%20v2.2.pdf) | 
| Argo floats vocabulary | An ontology to describe Argo floats and their metadata to semantically fetch data. | https://co.ifremer.fr/co//argo-linked-data/doc/argo-floats.ttl | argo : http://www.argodatamgt.org/argo-ontology# | Argo floats in-situ measurements | [Schema image](https://co.ifremer.fr/co//argo-linked-data/doc/argo_ontology_prototype.png) |

# Transverse complementary ontologies.

| Ontology | Domain | URL spec. | namespace | URI |
|----------|--------|-----------|-----------|-----|
| Data Quality Vocabulary | DCAT extension to cover quality of the data | https://www.w3.org/TR/vocab-dqv/ | dqv | http://www.w3.org/ns/dqv# |
| Provenance ontology | Ontology used to represent and interchange provenance information generated in different systems and under different contexts. | https://www.w3.org/TR/prov-o/ | prov | http://www.w3.org/ns/prov# |

