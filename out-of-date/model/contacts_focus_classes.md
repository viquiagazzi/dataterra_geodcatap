# Focus sur la modélisation des contacts en geodcat-ap

## Exemples catalogue Aeris "": 
 
+ Metadata API : https://services.aeris-data.fr/aeriscatalogueprod/metadata/956a4530-2d59-3aff-a4e5-83d5e7ff7a95
+ Catalogue interface search : "01kmCLay"

## geodcat-ap classes for Contact infos

+ dcat:contactPoint (R) a vcard:Kind or subclasses vcard:Organisation, vcard:Individual
+ dct:publisher (O) a foaf:Agent
+ dct:creator (O) a foaf:Agent
+ geodcat:distributor (O) a foaf:Agent
+ geodcat:principalInvestigator (O) foaf:Agent
+ geodcat:processor (O) foaf:Agent
