# Validité transformation métadonnées SAFE vers GeoDCAT-AP pour produit S2A_MSIL1C


| Granularité        | élément validation                           | Détail                                                         | Exemple (1)                   |
|--------------------|----------------------------------------------|----------------------------------------------------------------|-------------------------------|
| Image / Collection | Namespaces : recommandations                 | Préfixe ontologie gsp                                          | #L1                           |
|                    | Namespaces : recommandations                 | Ontologie geodcat pas instancié                                | ---                           |
|                    | Namespaces : recommandations                 | Ontologie gmi pas instancié                                    | ---                           |
|                    | Typage classe Dataset                        | Typer selon classe dcat:Dataset                                | #L12                          |
|                    | Typage range propriété dct:publisher         | Typer selon classe foaf:Agent                                  | #L32                          |
|                    | Présence de doublon dct:spatial              | Recommandation utiliser dcat:bbox (R), encodage en gml ou wkt. | #L37                          |
|                    | Typage range propriété dct:creator           | Typer selon classe foaf:Agent                                  | Propriété (O) pour Data Terra |
|                    | Typage range propriété dct:temporal          | Typer selon classe dct:PeriodOfTime                            | #L39                          |
|                    | Propriétés (R) de la classe dct:PeriodOfTime | Propriétés (R) : dcat:startDate et dcat:endDate.               | #L40, #L41                    |
|                    | Propriété dct:contributor                    | Pas présente dans les specs geodcat-ap v2.0                    | ---                           |
|                    | dcat:downloadURL chemin en local             | Lien directe sous instance_ID PEPS/sentinel-hub                | Propriété (O) pour Data Terra |
|                    | Propriété dcat:qualifiedRelation             | Bonne instanciation pour chaque Bx                             | #L47, #L51                    |
| Bandes / Item      | Propriété dcat:byteSize                      | Manquante dans exemple transformation                          | Propriété (O) pour Data Terra |

(1) ligne du fichier exemple : https://gitlab.com/viquiagazzi/dataterra_geodcatap/-/blob/main/examples/sentinel2/latest/example_S2A_MSIL1C.ttl
